import Network from '../Common/Api';

const orderActions = {
    getOrdersList: () => {
        return async dispatch => {
            dispatch({ type: "ORDER_LOADING", payload: {} });
            try {
                let orderList = await Network.get('/Request.php', {
                    params: {
                        type: 'GET_ORDERS'
                    }
                });

                dispatch({ type: "GET_ORDER_LIST_SUCCESS", payload: orderList.data });

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    }
};

export default orderActions;