import React from 'react';
import { connect } from "react-redux";
import { LoadingOverlay, Loader } from 'react-overlay-loader';
import 'react-overlay-loader/styles.css';
import { orderConstants } from './OrderConstants';
class OrderList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: this.props.loading,
            orderList: []
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            loading: nextProps.orders.loading,
            orderList: nextProps.orders.orderList
        });
    }

    render() {

        return (
            <div className="OrderList">

                <div className="container-fluid px-5 py-3">
                    <h3>Order List</h3>
                    <LoadingOverlay>
                        <Loader loading={this.state.loading} />

                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Order ID</th>
                                    <th>Customer Name</th>
                                    <th>Mobile</th>
                                    <th>Order Amount</th>
                                    <th>Order Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.loading ? null :
                                    (this.state.orderList !== undefined && this.state.orderList.length === 0) ? (<tr><td colSpan="6" style={{ textAlign: "center" }}>No Data Available</td></tr>) :
                                        this.state.orderList.map((datas, index) => {
                                            const { id, customerName, phone, amount, status } = datas;
                                            return (
                                                <tr key={id}>
                                                    <td>{index + 1}</td>
                                                    <td>{id}</td>
                                                    <td>{customerName}</td>
                                                    <td>{phone}</td>
                                                    <td>{amount}</td>
                                                    <td>{orderConstants[status]}</td>
                                                </tr>
                                            );

                                        })}
                            </tbody>
                        </table>
                    </LoadingOverlay>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        orderList: state.orders.orderList,
        orders: state.orders
    }
}

export default connect(mapStateToProps)(OrderList);
