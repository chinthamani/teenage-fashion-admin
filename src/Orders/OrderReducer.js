function OrderReducer(state = {
}, actions) {
    
    switch (actions.type) {
        
        case "ORDER_LOADING": {
            return {
                loading : true
            };
        }

        case "GET_ORDER_LIST_SUCCESS": {
            return {
                orderList : actions.payload,
                loading : false
            };
        }
        default: return state;
    }
}

export default OrderReducer;
