import React, { Component } from 'react';
import { connect } from "react-redux";
import OrderList from './OrderList';
import actions from "./OrderActions";

class Orders extends Component {
  componentDidMount() {
    this.props.dispatch(actions.getOrdersList());
  }
  render() {
    return (
      <div className="Orders">
        <OrderList/>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state);
  return {
    orderList: state.orders
  }
}
export default connect(mapStateToProps)(Orders);
