export const orderConstants = {
    1: 'Order Received',
    2: 'Order Confirmed',
    3: 'Dispatched',
    4: 'Delivered'
};