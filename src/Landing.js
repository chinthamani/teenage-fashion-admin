import React, { Component } from 'react';
import Header from './Common/Header';
import './resources/css/style.css';
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.css';
import "mdbreact/dist/css/mdb.css";

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Category from './Categories/Category';

import AddProduct from './Products/AddProduct';
import ProductList from './Products/ProductList';
import Login from './Login/Login';
import Logout from './Login/Logout';
import PrivateRoute from './Common/PrivateRoute';
import { connect } from "react-redux";
import Orders from './Orders/Orders';

class Landing extends Component {
    render() {
        return (
            <div className="Landing">

                <Router>
                    <Header />
                    <Switch>
                        <Route exact path="/Admin" component={Login} />
                        <PrivateRoute path="/Admin/AddCategory" component={Category} auth={this.props.auth} />
                        {/* <Route path="/Admin/AddCategory" component={Category} /> */}
                        <PrivateRoute path="/Admin/AddProduct" component={AddProduct} auth={this.props.auth} />
                        {/* <Route path="/Admin/AddProduct" component={AddProduct} /> */}
                        <PrivateRoute path="/Admin/?suc=add" component={AddProduct} auth={this.props.auth} />
                        {/* <Route path="/Admin/?suc=add" component={AddProduct} /> */}
                        <PrivateRoute path="/Admin/ViewProducts" component={ProductList} auth={this.props.auth} />
                        {/* <Route path="/Admin/ViewProducts" component={ProductList} /> */}
                        <PrivateRoute path="/Admin/Logout" component={Logout} auth={this.props.auth} />
                        {/* <Route path="/Admin/Logout" component={Logout} /> */}
                        <PrivateRoute path="/Admin/Orders" component={Orders} auth={this.props.auth} />
                    </Switch>
                </Router>

            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        auth: state.login
    }
}

export default connect(mapStateToProps)(Landing);

