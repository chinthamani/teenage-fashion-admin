import Network from '../Common/Api';

const categoryActions = {
    getTestData: () => {
        return async dispatch => {
            try {
                let response = await Network.get('/Request.php', {
                    params: {
                        type: 'GET_CATEGORIES_FEMALE'
                    }
                });
                dispatch({ type: "GET_TEST_DATA_SUCCESS", payload: response.data });
            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    },
    addCategory: (obj) => {
        return async dispatch => {
            dispatch({ type: "ADD_CATEGORY_REQUEST", payload: {} });
            try {
                let response = await Network.get('/Request.php', {
                    params: {
                        type: 'ADD_CATEGORY',
                        catList: JSON.stringify(obj)
                    }
                });
                if (response.data) {
                    let catList = await Network.get('/Request.php', {
                        params: {
                            type: 'GET_CATEGORIES'
                        }
                    });


                    dispatch({ type: "GET_ADD_CATEGORY_SUCCESS", payload: catList.data });
                }
            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    },
    deleteCategory: (id) => {
        return async dispatch => {
            dispatch({ type: "DELETE_CATEGORY_REQUEST", payload: {} });
            try {
                let response = await Network.get('/Request.php', {
                    params: {
                        type: 'DELETE_CATEGORY',
                        id: id
                    }
                });
                if (response.data) {
                    let catList = await Network.get('/Request.php', {
                        params: {
                            type: 'GET_CATEGORIES'
                        }
                    });

                    dispatch({ type: "GET_DELETE_CATEGORY_SUCCESS", payload: catList.data });
                }
            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    },


    getCategoryList: () => {
        return async dispatch => {
            dispatch({ type: "DELETE_CATEGORY_REQUEST", payload: {} });
            try {
                let catList = await Network.get('/Request.php', {
                    params: {
                        type: 'GET_CATEGORIES'
                    }
                });

                dispatch({ type: "GET_CATEGORY_LIST_SUCCESS", payload: catList.data });

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    }
};

export default categoryActions;