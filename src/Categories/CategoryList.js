import React from 'react';
import actions from "./CategoryActions";
import { connect } from "react-redux";
import { LoadingOverlay, Loader } from 'react-overlay-loader';
import 'react-overlay-loader/styles.css';

class CategoryList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: this.props.loading,
            categoryList: []
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        this.setState({
            loading: nextProps.category.loading,
            categoryList: nextProps.category.categoryList
        });
    }

    deleteCategory = (id) => {
        this.props.dispatch(actions.deleteCategory(id));
    }


    render() {

        return (
            <div className="CategoryList">

                <div className="container-fluid px-5 py-3">
                    <h3>Category List</h3>
                    <LoadingOverlay>
                        <Loader loading={this.state.loading} />
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Category Type</th>
                                    <th>Category Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.loading ? null :
                                    (this.state.categoryList !== undefined && this.state.categoryList.length === 0) ? (<tr><td colSpan="6" style={{ textAlign: "center" }}>No Data Available</td></tr>) :
                                        this.state.categoryList.map((datas, index) => {
                                            const { id, name, type } = datas;
                                            return (
                                                <tr key={id}>
                                                    <td>{index + 1}</td>
                                                    <td>{type === 'M' ? 'Male' : 'Female'}</td>
                                                    <td>{name}</td>
                                                    <td><input type="button" onClick={() => this.deleteCategory(id)} className="btn btn-primary" value="Delete" /></td>
                                                </tr>
                                            );

                                        })}

                            </tbody>
                        </table>
                    </LoadingOverlay>
                </div>


            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        categoryList: state.category.categoryList,
        category: state.category
    }
}

export default connect(mapStateToProps)(CategoryList);
