import React, { Component } from 'react';
import { connect } from "react-redux";
import AddCategory from './AddCategory';
import CategoryList from './CategoryList';
import actions from "./CategoryActions";

class Category extends Component {
  componentDidMount() {
    this.props.dispatch(actions.getCategoryList());
  }
  render() {
    return (
      <div className="Category">
        <AddCategory />
        <CategoryList deleteCategory={id=>this.props.dispatch(actions.deleteCategory(id))}/>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state);
  return {
    categoryList: state.category
  }
}
export default connect(mapStateToProps)(Category);
