import React, { Component } from 'react';
import actions from "./CategoryActions";
import { connect } from "react-redux";
class AddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categorytype: "M",
            categoryName: ""
        }
    }

    changeState = (e, val) => {
        this.setState({
            [val]: e.target.value
        });
    }

    addCategory = (e) => {
        e.preventDefault();
        document.getElementById("addCategoryForm").className += " was-validated";
        if (this.state.categoryName) {
            let catArr = [];
            let catObj = {
                catType: this.state.categorytype,
                catName: this.state.categoryName
            }
            catArr.push(catObj);
            this.props.dispatch(actions.addCategory(catArr));
            this.setState({
                categoryName: ""
            });
            document.getElementById("addCategoryForm").className = "contact-form";
        }
    }

    clearForm = (e) => {
        e.preventDefault();
        document.getElementById("addCategoryForm").className = "contact-form";
        this.setState({
            categoryName: ""
        })
    }

    render() {
        return (
            <div className="AddCategory">

                <div className="p-1">
                    <form className="contact-form" id="addCategoryForm" onSubmit={(e) => this.addCategory()}>
                        <div className="container">
                            <div className="row py-5">
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Category Type</label>
                                    <div className="form-group">
                                        <select className="form-control" onChange={(e) => this.changeState(e, 'categorytype')} required>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Category Name</label>
                                    <div className="form-group">
                                        <input type="text" className="form-control" value={this.state.categoryName} onChange={(e) => this.changeState(e, 'categoryName')} required />
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-6 col-6">
                                    <br />
                                    <div className="form-group">
                                        <button className="site-btn" onClick={(e) => this.addCategory(e)}>Add</button>
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-6 col-6">
                                    <br />
                                    <div className="form-group">
                                        <button onClick={(e) => this.clearForm(e)} className="site-btn-default">Clear</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        );
    }

}
const mapStateToProps = (state) => {
    console.log(state);
    return {
        categoryList: state.category
    }
}
export default connect(mapStateToProps)(AddCategory);