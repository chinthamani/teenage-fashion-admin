function CategoryReducer(state = {
}, actions) {
    
    switch (actions.type) {
        case "GET_TEST_DATA_SUCCESS": {
            return state = actions.payload;
        }
        case "GET_ADD_CATEGORY_SUCCESS": {
            return {
                categoryList : actions.payload,
                loading : false
            };
        }
        case "GET_DELETE_CATEGORY_SUCCESS": {
            return {
                categoryList : actions.payload,
                loading : false
            };
        }
        case "ADD_CATEGORY_REQUEST": {
            return {
                loading: true
            };
        }
        case "DELETE_CATEGORY_REQUEST": {
            return {
                loading: true
            };
        }
        case "GET_CATEGORY_LIST_SUCCESS": {
            return {
                categoryList : actions.payload,
                loading : false
            };
        }
        default: return state;
    }
}

export default CategoryReducer;
