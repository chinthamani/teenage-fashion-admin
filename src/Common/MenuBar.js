import React, { Component } from 'react';
import {
    MDBNavbar, MDBNavbarNav, MDBNavItem, MDBNavbarToggler, MDBCollapse,
    MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem
} from 'mdbreact';
import { NavLink, Link } from 'react-router-dom';
export default class MenuBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        return (<MDBNavbar dark expand="md">

            <MDBNavbarToggler onClick={this.toggleCollapse} />
            <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                <MDBNavbarNav left>
                    <MDBNavItem>
                        <NavLink className="nav-link Ripple-parent" activeClassName="active" to="/Admin/AddCategory">Category</NavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                        <MDBDropdown>
                            <MDBDropdownToggle nav caret>
                                <span className="mr-2">Products</span>
                            </MDBDropdownToggle>
                            <MDBDropdownMenu className="dropdown-default">

                                <MDBDropdownItem componentclass='' style={{ padding: '0px' }}> <NavLink tabIndex="0" className="dropdown-item" activeClassName="active" to="/Admin/AddProduct">Add Product</NavLink></MDBDropdownItem>

                                <MDBDropdownItem componentclass='' style={{ padding: '0px' }}>   <NavLink tabIndex="0" className="dropdown-item" activeClassName="active" to="/Admin/ViewProducts">View Products</NavLink></MDBDropdownItem>
                            </MDBDropdownMenu>
                        </MDBDropdown>
                    </MDBNavItem>
                    <MDBNavItem>
                        <NavLink className="nav-link Ripple-parent" activeClassName="active" to="/Admin/Orders">Orders</NavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                        <NavLink className="nav-link Ripple-parent" activeClassName="active" to="/Admin/Users">Users</NavLink>
                    </MDBNavItem>

                </MDBNavbarNav>
                <MDBNavbarNav right>


                    <div className="md-form my-0">
                        <MDBNavItem>
                            <Link className="nav-link Ripple-parent" to="/Admin/Logout">Logout</Link>
                        </MDBNavItem>
                    </div>


                </MDBNavbarNav>
            </MDBCollapse>
        </MDBNavbar>);
    }
}