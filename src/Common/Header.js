import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../resources/img/logo.png';
import MenuBar from './MenuBar';
import "mdbreact/dist/css/mdb.css";
import { connect } from "react-redux";

class Header extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loggedIn : this.props.login.loggedIn
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.login.loggedIn){
            this.setState({
                loggedIn : true
            })
        }else{
            this.setState({
                loggedIn : false
            })
        }
    }
    render() {
        return (
            <div className="Header">
                <div className="header-top">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-2 text-left text-lg-left mt-2">

                                <Link to="/Admin" className="site-logo">
                                    <img src={logo} alt="" />
                                </Link>
                            </div>
                            <div className="col-xl-6 col-lg-5">

                            </div>
                            <div className="col-xl-4 col-lg-5">

                            </div>
                        </div>
                    </div>
                </div>
              {this.state.loggedIn ?  <MenuBar />:null}
            </div>
        );
    }

}
const mapStateToProps = (state) => {
    console.log(state);
    return {
      login: state.login
    }
  }
  
export default connect(mapStateToProps)(Header);