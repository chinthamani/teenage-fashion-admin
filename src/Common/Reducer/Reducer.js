import { combineReducers } from 'redux';
import CategoryReducer from '../../Categories/CategoryReducer';
import ProductsReducer from '../../Products/ProductsReducer';
import { LoginReducer } from '../../Login/LoginReducer';
import OrderReducer from '../../Orders/OrderReducer';

export const reducer = combineReducers({
    category : CategoryReducer,
    product : ProductsReducer,
    login : LoginReducer,
    orders : OrderReducer
});