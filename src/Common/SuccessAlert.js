import React from 'react';

class SuccessAlert extends React.Component {

    render() {
        return (
            <div className="SuccessAlert">
                {this.props.successFlag ? (<div className="alert alert-success">{this.props.successMsg}</div>) : ""}
            </div>
        );
    }
}
export default SuccessAlert;