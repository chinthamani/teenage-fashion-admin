import React from 'react';

class ErrorAlert extends React.Component {

    render() {
        return (
            <div className="ErrorAlert">
                {this.props.errorFlag ? (<div className="alert alert-danger">{this.props.errorMsg}</div>) : ""}
            </div>
        );
    }
}
export default ErrorAlert;