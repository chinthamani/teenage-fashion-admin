import React from 'react';
import Landing from './Landing';
import { Provider } from 'react-redux';
import { store } from './Common/Store/Store';
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Landing />
      </Provider>
    </div>
  );
}

export default App;
