
let user = JSON.parse(localStorage.getItem('tfadminuser'));
const initialState = user ? { loggedIn: true, user } : {};

export function LoginReducer(state = initialState, action) {
  console.log(action);
  switch (action.type) {
    case "ADMIN_LOGIN_REQUEST":
      console.log("LOGIN request");
      return {
        loggingIn: true
      };
    case "LOGIN_SUCCESS":
      return {
        loggedIn: true,
        loggingIn: false,
        login: action.payload
      };
      case "LOGIN_FAILED":
      return {
        login: action.payload,
        loggingIn: false,
        invalidLogin : true
      };
    case "LOGOUT_SUCCESS":
      return {
        loggedIn: false
      };
    case "USERS_LOGOUT":
      return {};
    default:
      return state
  }
}