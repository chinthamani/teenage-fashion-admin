import React from 'react';
import {logout} from './LoginActions';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
class Logout extends React.Component {

    constructor(props) {
        super(props);
        this.props.logout();
    }

    componentDidMount(){

        this.props.history.push("/Admin");
    }

    render() {
        return (
            <div className="Logout">
                
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    console.log(state);
    return {
        login: state.login
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        logout,
    },dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Logout);