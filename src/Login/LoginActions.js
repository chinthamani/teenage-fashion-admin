import Network from '../Common/Api';


 export const getLogin = (username, password) => {
        return async dispatch => {
            
            try {
                dispatch({type : "ADMIN_LOGIN_REQUEST" , payload : {}});
                let response = await Network.get('/Request.php', {
                    params: {
                        type: 'GET_LOGIN_ADMIN',
                        username : username,
                        password : password
                    }
                });
                let loginResponse = JSON.stringify(response.data);
                
                if(JSON.parse(loginResponse)[0] === 'F'){
                    dispatch({ type: "LOGIN_FAILED", payload: response.data });
                }
                else{
                    localStorage.setItem('tfadminuser', JSON.stringify(response.data));
                    dispatch({ type: "LOGIN_SUCCESS", payload: response.data });
                }

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    }

    export const logout = () => {
        return async dispatch => {
            try {
                localStorage.removeItem("tfadminuser");
                dispatch({type : "LOGOUT_SUCCESS", payload : null});

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
       
    }



