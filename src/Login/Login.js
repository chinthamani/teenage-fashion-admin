import React from 'react';
import {getLogin} from './LoginActions';
import { connect } from "react-redux";
import { LoadingOverlay, Loader } from 'react-overlay-loader';

import 'react-overlay-loader/styles.css';
import { bindActionCreators } from 'redux';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            loggingIn: false,
            submitted: false
        }
    }

    componentDidMount() {
        if (this.props.login.loggedIn) {
            this.props.history.push("/Admin/AddCategory");
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.login !== nextProps.login) {
            this.setState({
                loggingIn: nextProps.login.loggingIn
            })
            if (nextProps.login.loggedIn) {
                
                this.props.history.push('/Admin/AddCategory');
            } else {
                this.props.history.push('/Admin');
            }
        }
    }
    changeState = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleLogin = (e) => {
        e.preventDefault();
        document.getElementById("loginForm").className += " was-validated";
        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password) {
            //this.props.dispatch(actions.login(username, password));
            this.props.getLogin(username,password);
        }

    }
    render() {
        return (
            <div className="Login">
                <LoadingOverlay>
                <Loader fullPage loading={this.state.loggingIn} />
                </LoadingOverlay>

                    <form id="loginForm" className="contact-form needs-validation" onSubmit={(e) => this.handleLogin(e)} noValidate>
                        <div className="container">

                            <div className="row pt-5">

                                <div className="col-lg-4 col-md-6 col-12 mx-auto">
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Username" name="username" value={this.state.username} onChange={(e) => this.changeState(e)} required />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-4 col-md-6 col-12 mx-auto">
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Password" name="password" value={this.state.password} onChange={(e) => this.changeState(e)} required />
                                    </div>
                                </div>
                            </div>
                            {this.props.login.invalidLogin ?
                                (<div className="row">

                                    <div className="col-lg-4 col-md-6 col-12 text-center mx-auto">
                                        <div className="form-group">
                                            <span className="error">Invalid Login</span>
                                        </div>
                                    </div>
                                </div>) : null}
                            <div className="row">
                                <div className="col-lg-2 col-md-6 col-6 mx-auto">
                                    <div className="form-group">
                                        <button className="site-btn">Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
               

            </div>
        );
    }
}
const mapStateToProps = (state) => {
    console.log(state);
    return {
        login: state.login
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getLogin,
    },dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);