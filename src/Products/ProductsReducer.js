function ProductsReducer(state = {
}, actions) {
    
    switch (actions.type) {
        case "FETCH_CATEGORY_LIST_SUCCESS": {
            return state = actions.payload;
        }
        case "GET_PRODUCT_LIST_SUCCESS": {
            return state = actions.payload;
        }
        
        default: return state;
    }
}

export default ProductsReducer;
