import Network from '../Common/Api';

const productActions = {
   
    fetchCategoryList : (catType) => {
        return async dispatch => {

            try {
                let response = await Network.get('/Request.php', {
                    params: {
                        type: 'FETCH_CATEGORY_LIST',
                        catType : catType
                    }
                });

                dispatch({ type: "FETCH_CATEGORY_LIST_SUCCESS", payload: response.data });

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    },
    deleteProduct : (id) => {
        return async dispatch => {

            try {
                let response = await Network.get('/Request.php', {
                    params: {
                        type: 'DELETE_PRODUCT',
                        id: id
                    }
                });

                dispatch({ type: "GET_DELETE_PRODUCT_SUCCESS", payload: response.data });

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    },
    
    
    getProductList: () => {
        return async dispatch => {

            try {
                let prodList = await Network.get('/Request.php', {
                    params: {
                        type: 'GET_PRODUCTS'
                    }
                });

                dispatch({ type: "GET_PRODUCT_LIST_SUCCESS", payload: prodList.data });

            } catch (error) {
                // dispatch({ type: "GET_TEST_DATA_FAILED", payload: error.message });
            }
        }
    }
};

export default productActions;