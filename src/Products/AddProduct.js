import React, { Component } from 'react';
import actions from "./ProductsActions";
import { connect } from "react-redux";
import SuccessAlert from '../Common/SuccessAlert';
import ErrorAlert from '../Common/ErrorAlert';
import { LoadingOverlay, Loader } from 'react-overlay-loader';

import 'react-overlay-loader/styles.css';
class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: {
                value: "",
                valid: false
            },
            name: {
                value: "",
                valid: false
            },
            price: {
                value: "",
                valid: false
            },
            description: {
                value: "",
                valid: false
            },
            categoryType: "M",
            categoryId: {
                value: 0,
                valid: false
            },
            categoryList: [],
            successFlag: false,
            successMsg: "",
            errorFlag: false,
            errorMsg: "",
            loading: true


        }
    }


    componentDidMount() {
        this.props.dispatch(actions.fetchCategoryList('M'));
        setTimeout(
            () => {
                this.setState({ loading: false });
            },
            500
        );
        if (this.props.location.search === '?suc=add') {
            this.setState({
                successFlag: true,
                successMsg: "Product Added successfulluy"
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.catList !== nextProps.catList) {
            this.setState({
                categoryList: nextProps.catList
            });
        }
    }

    fetchCategoryList = (e) => {
        let type = e.target.value;

        this.setState({
            categoryType: e.target.value
        });

        this.props.dispatch(actions.fetchCategoryList(type));

    }

    changeState = (event, val) => {
        this.setState({ [val]: { value: event.target.value, valid: !!event.target.value } });
    }

    submitHandler = (event) => {
        document.getElementById("addProductForm").className += " was-validated";
    };


    clearForm = (e) => {
        e.preventDefault();
        document.getElementById("addProductForm").className = "contact-form";
        this.props.dispatch(actions.fetchCategoryList(this.state.categoryType));
        this.setState({
            code: {
                value: "",
                valid: false
            },
            name: {
                value: "",
                valid: false
            },
            price: {
                value: "",
                valid: false
            },
            description: {
                value: "",
                valid: false
            },
            categoryType: "M",
            categoryId: {
                value: 0,
                valid: false
            },
            categoryList: []
        });
    }

    render() {
        return (
            <div className="AddProduct">
                <LoadingOverlay>
                    <Loader fullPage loading={this.state.loading} />
                </LoadingOverlay>
                <SuccessAlert successMsg={this.state.successMsg} successFlag={this.state.successFlag} />
                <ErrorAlert errorMsg={this.state.errorMsg} errorFlag={this.state.errorFlag} />
                <div className="p-1">
                    <form className="contact-form needs-validation" id="addProductForm" method="post" encType="multipart/form-data" action="https://www.teenagefashion.in/API/Database/Submit.php">
                        <input type="hidden" name="type" value="ADD_PRODUCT" />
                        <div className="container">
                            <div className="row py-5">
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Select Category Type</label>
                                    <div className="form-group">
                                        <select name="categoryType" onChange={(e) => this.fetchCategoryList(e)}>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Select Category</label>
                                    <div className="form-group">
                                        <select name="categoryId" className="form-control" onChange={(e) => this.changeState(e, 'categoryId')} required>
                                            <option value="">--SELECT--</option>
                                            {this.state.categoryList.map(value => <option key={value.id} value={value.id}>{value.name}</option>)}
                                        </select>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Product Code</label>
                                    <div className="form-group">
                                        <input type="text" name="code" className="form-control" value={this.state.code.value} onChange={(e) => this.changeState(e, 'code')} required />

                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Product Name</label>
                                    <div className="form-group">
                                        <input type="text" name="name" className="form-control" value={this.state.name.value} onChange={(e) => this.changeState(e, 'name')} required />
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Price</label>
                                    <div className="form-group">
                                        <input type="number" name="price" className="form-control number" value={this.state.price.value} onChange={(e) => this.changeState(e, 'price')} required />
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Images</label>
                                    <div className="form-group file-field">
                                        <input type="file" name="prodImages[]" className="form-control" multiple required />
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-12">
                                    <label>Description</label>
                                    <div className="form-group">
                                        <textarea name="description" col="2" row="3" className="form-control" value={this.state.description.value} onChange={(e) => this.changeState(e, 'description')} required>{this.state.description}</textarea>
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-6 col-6">
                                    <br />
                                    <div className="form-group">
                                        <button type="submit" onClick={(e) => this.submitHandler(e)} className="site-btn" >Add</button>
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-6 col-6">
                                    <br />
                                    <div className="form-group">
                                        <button onClick={(e) => this.clearForm(e)} className="site-btn-default">Clear</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

}
const mapStateToProps = (state) => {

    return {
        catList: state.product
    }
}
export default connect(mapStateToProps)(AddProduct);