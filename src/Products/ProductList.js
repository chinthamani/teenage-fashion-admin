import React from 'react';
import { ClipLoader } from 'react-spinners';
import actions from "./ProductsActions";
import { connect } from "react-redux";

class ProductList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            productList: []
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.getProductList());
    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            loading: false,
            productList: nextProps.productList
        });

    }

    deleteProduct = (id) => {
        this.props.dispatch(actions.deleteProduct(id));
        this.props.dispatch(actions.getProductList());
    }

    render() {

        return (
            <div className="ProductList">
                <div className="container-fluid px-5 py-3">
                    <h3>Product List</h3>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th className="hide">Category</th>
                                <th>Product Code</th>
                                <th >Name</th>
                                <th className="hide">Price</th>
                                <th className="hide">Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            {this.state.loading ? <tr><td colSpan="7" align="center"><ClipLoader color={'#123abc'} size={50} loading={true} /></td></tr> :
                                (this.state.productList.length === 0 && this.state.loading === false) ? (<tr><td colSpan="7" style={{ textAlign: "center" }}>No Data Available</td></tr>) :
                                    this.state.productList.map((datas, index) => {
                                        const { id, code, name, description, price, type, catName } = datas;
                                        return (
                                            <tr key={id}>
                                                <td>{index + 1}</td>
                                                <td className="hide">{type === 'M' ? 'Male' : 'Female'} - {catName}</td>
                                                <td>{code}</td>
                                                <td >{name}</td>
                                                <td className="hide">{price}</td>
                                                <td className="hide">{description}</td>
                                                <td><input type="button" onClick={() => this.deleteProduct(id)} className="btn btn-primary" value="Delete" /></td>
                                            </tr>
                                        );

                                    })}

                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        productList: state.product
    }
}

export default connect(mapStateToProps)(ProductList);
